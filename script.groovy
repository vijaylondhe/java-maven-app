def buildJar(){
    echo 'building the jar file'
    sh 'mvn package'
}
def buildImage(){
    echo 'Building application image'
        withCredentials([usernamePassword(credentialsId: 'dockerhub-credentials', usernameVariable: 'USER' , passwordVariable: 'PASSWORD')]){
        sh 'docker build -t vijay815/demo-app:jma-2.0 .'
        sh "echo $PASSWORD | docker login -u $USER --password-stdin"  
        sh 'docker push vijay815/demo-app:jma-2.0'
    }
}
def deployApp(){
    echo 'Deploying Application..'
}
return this 
